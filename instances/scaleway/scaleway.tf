provider "scaleway" {
  region       = "fr-par"
  zone         = "fr-par-1"
}

resource "scaleway_instance_ip" "public_ip" {}

resource "scaleway_instance_server" "web" {
  type  = "GP1-XS"
  image = "2eec1a1e-5d39-49c8-8cee-35f284ea08c3"

  ip_id = scaleway_instance_ip.public_ip.id

}
