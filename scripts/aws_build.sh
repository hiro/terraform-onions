#!/bin/bash
set -e

# Install necessary dependencies
sudo apt-get upgrade 
sudo apt-get update
sudo apt -y -qq install tor nginx

# Setup sudo to allow no-password sudo for "peerorg" group and adding "peer" user
sudo groupadd -r peerorg
sudo useradd -m -s /bin/bash peer
sudo usermod -a -G peerorg peer
sudo cp /etc/sudoers /etc/sudoers.orig
echo "peer  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/peer

# Installing SSH key
sudo mkdir -p /home/peer/.ssh
sudo mkdir /srv/onion_web_service/
sudo chmod 700 /home/peer/.ssh
sudo cp /tmp/aws_id_rsa.pub /home/peer/.ssh/authorized_keys
sudo chmod 600 /home/peer/.ssh/authorized_keys
sudo chown -R peer /home/peer/.ssh
sudo chown -R peer /srv/onion_web_service
sudo chmod 700 /srv/onion_web_service

# Copy html files to /var/www/html
sudo cp /tmp/static-html/index.html /var/www/html

# Copy tor configuration file torrc
sudo cp /tmp/tor/torrc /etc/tor/torrc
sudo service tor stop

# Copy nginx configuration file
sudo cp /tmp/nginx/nginx.conf /etc/nginx/nginx.conf
sudo nginx -s stop

# Check user peer

sudo -H -i -u peer -- env bash << EOF
whoami
echo ~peer

cd /home/peer

tor
sleep 60

EOF

sudo sed -i "s/ONION/$(sudo cat /srv/onion_web_service/hostname)/g" /usr/share/nginx/html/index.html
sudo sed -i "s/ONION/$(sudo cat /srv/onion_web_service/hostname)/g" /etc/nginx/nginx.conf

echo "This is the onion address: $(sudo cat /srv/onion_web_service/hostname)"

sudo nginx

sudo /usr/sbin/update-rc.d -f nginx defaults
sudo /usr/sbin/update-rc.d -f tor defaults
