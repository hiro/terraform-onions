#!/bin/sh
set -e

tor
sleep 60

sed -i "s/ONION/$(cat /srv/onion_web_service/hostname)/g" /usr/share/nginx/html/index.html
sed -i "s/ONION/$(cat /srv/onion_web_service/hostname)/g" /etc/nginx/nginx.conf

echo "This is the onion address: $(cat /srv/onion_web_service/hostname)"

nginx

/usr/sbin/update-rc.d -f nginx defaults
/usr/sbin/update-rc.d -f tor defaults
