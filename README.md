This repository contains configuration to deploy a simple onion service via
Terraform and Packer to different infrastructure.

To build an image:

```
$ packer build images/<image>

```
To deploy an image:
```
$ cd instances/<cloud>
$ terraform init
$ terraform apply
```

You can destroy the machine created with:
```
$ terraform destroy
```

# Scaleway

You will need the following ENV variable setup:

```
scaleway_organization_id
scaleway_secret_key
scaleway_access_key

SCW_ACCESS_KEY
SCW_SECRET_KEY
SCW_DEFAULT_ORGANIZATION_ID

```


# AWS

You will need the following ENV variable setup:

```
aws_access_key
aws_secret_key

AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION

```

Connect via ssh:
```
$ ssh peer@$(echo "aws_instance.web.public_ip" | terraform console) -i /home/user/.ssh/aws_id_rsa
```
